from django.db import models

# Create your models here.

class album(models.Model):
    id = models.CharField(max_length=128,primary_key=True)
    name = models.CharField(max_length=64)
    release_date = models.DateField()
    release_date_precision = models.CharField(max_length=8)
    total_tracks = models.IntegerField()
    artists = models.CharField(max_length=64)
    href = models.CharField(max_length=256)
    images_url = models.CharField(max_length=256)

    def __str__(self):
        return str(self.id)+"-"+str(self.name)
