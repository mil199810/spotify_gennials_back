from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from .views import  ListAlbums
urlpatterns = [
    path('list_albums/<str:searchTerm>', csrf_exempt(ListAlbums.as_view()), name='list_albums'),
]