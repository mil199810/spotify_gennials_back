#from django.shortcuts import render
import base64
import requests
from django.http import JsonResponse
from django.views import View
from api.models import album as tbl_album

class ListAlbums(View):
    def get(self, request, *args, **kwargs):
        if request.method == "GET":
            url = "https://accounts.spotify.com/api/token"
            headers = {}
            data = {}
            clientId="7e57268103844409a57ffda1313b72a2"
            clientSecret="576c2c17aea44058a1a4129276bb3521"
            # Encode as Base64
            message = f"{clientId}:{clientSecret}"
            messageBytes = message.encode('ascii')
            base64Bytes = base64.b64encode(messageBytes)
            base64Message = base64Bytes.decode('ascii')
            headers['Authorization'] = f"Basic {base64Message}"
            data['grant_type'] = "client_credentials"
            r = requests.post(url, headers=headers, data=data)
            token = r.json()['access_token']
            print(token)
            # Step 2 - Use Access Token to call playlist endpoint
            almbumlistId = self.kwargs['searchTerm']
            almbumlistUrl = f"https://api.spotify.com/v1/search?q={almbumlistId}&type=album"
            headers = {
                "Authorization": "Bearer " + token
            }
            res = requests.get(url=almbumlistUrl, headers=headers)
            albums=res.json()['albums']['items']
            for album in albums:
                db_album, created = tbl_album.objects.get_or_create(
                        id = album['id'],
                        name = album['name'],
                        release_date = album['release_date'],
                        release_date_precision = album['release_date_precision'],
                        total_tracks = album['total_tracks'],
                        artists = album['artists'][0]['name'],
                        href = album['href'],
                        images_url = album['images'][0]['url'])
            return JsonResponse(data=res.json(),safe=False)
